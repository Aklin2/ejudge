#include <stdio.h>
int main(){
    int a, b, n, i, j;
    scanf("%d%d%d", &a, &b, &n);
    printf("%*s ", n, "");
    for(i = a; i < b; i++){
        printf((i == b-1) ? "%*d\n" : "%*d ", n, i);
    }
    for(i = a; i < b; i++){
        printf("%*d ", n, i);
        for(j = a; j < b; j++){
            printf((j == b-1) ? "%*d\n" : "%*d ", n, i*j); 
        }
    }
    return 0;
}

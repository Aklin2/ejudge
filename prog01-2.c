#include <stdio.h>
char c;
int s = 0;
int main(){
    while((c=getchar()) != EOF){
        if((c >= '0') && (c <= '9')) s += c - '0';
        else if(c >= 'a' && c <= 'f' ) s += c - 'a' + 10;
        else if(c >= 'A' && c <= 'F' ) s += c - 'A' + 10;
    }
    printf("%d\n", s);
    return 0; 
}

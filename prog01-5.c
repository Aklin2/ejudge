#include <stdio.h>
#include <stdlib.h>
typedef struct Node* tree;
typedef struct Node{
    int elem;
    int count;
    tree right;
    tree left;
} node;
void clear_tree(tree t){
    if(t == NULL) return;
    clear_tree(t->right);
    clear_tree(t->left);
    free(t);
}
void insert(tree* t, int n){
    #define p (*t)
    if(p == NULL){
        p = malloc(sizeof(node));
        p->elem = n;
        p->count = 1;
        p->right = NULL;
        p->left = NULL;
        return;
    }
    if(n < p->elem) insert(&(p->left), n);
    else if(n > p->elem) insert(&(p->right), n);
    else p->count +=1;
    return;
}
void print_tree(tree t){
    int i;
    if(t == NULL) return;
    print_tree(t->left);
    for(i = 0; i < t->count; i++) printf("%d ", t->elem);
    print_tree(t->right);
    return;
}
int main(int argc, char* argv[]){
    tree t = NULL;
    int i, n;
    FILE* f;
    if(argc == 1) return 0;
    for(i = 1; i < argc; i++){
        f = fopen(argv[i], "r");
        while(fscanf(f, "%d", &n) != EOF){
            insert(&t, n);
        }
        fclose(f);
    }
    print_tree(t);
    printf("\n");
    clear_tree(t);
    return 0;
}
